package br.com.casadocodigo.loja.daos;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.casadocodigo.loja.builders.ProdutoBuilder;
import br.com.casadocodigo.loja.conf.DataSourceConfigurationTest;
import br.com.casadocodigo.loja.conf.JPAConfiguration;
import br.com.casadocodigo.loja.models.Produto;
import br.com.casadocodigo.loja.models.TipoPreco;

//Classe criada para fazer os testes da classe ProdutoDAO. Por padrao de nomenclatura, sempre é utilizado o nome da classe + o sufixo "Test".
@RunWith(SpringJUnit4ClassRunner.class) //Annotation do JUnit para ele rodar junto com as classes de testes (SpringJUnit4ClassRunner) do Spring
@ContextConfiguration(classes={JPAConfiguration.class, ProdutoDAO.class, DataSourceConfigurationTest.class}) //Informa as classes necessarias para rodar os testes
@ActiveProfiles("test") //Diz ao Spring que o perfil dessa classe sera de testes, o que faz com que o bd de testes seja utilizado
public class ProdutoDAOTest {

	@Autowired
	private ProdutoDAO produtoDao;
	
	@Test //Anotação do JUnit para ele saber qual teste vai rodar aqui dentro
	@Transactional
	public void deveSomarTodosPrecosPorTipoLivro() {
		
		//Cria alguns objetos para teste (4 livros com o valor de 10 cada, para os tipos impresso e ebook)
		List<Produto> livrosImpressos = ProdutoBuilder.newProduto(TipoPreco.IMPRESSO, BigDecimal.TEN).more(3).buildAll();
		List<Produto> livrosEbook = ProdutoBuilder.newProduto(TipoPreco.EBOOK, BigDecimal.TEN).more(3).buildAll();
		
		livrosImpressos.stream().forEach(produtoDao::gravar);
		livrosEbook.stream().forEach(produtoDao::gravar);
		
		BigDecimal valor = produtoDao.somaPrecosPorTipo(TipoPreco.EBOOK);
		
		//Assert é do JUnit. O primeiro parametro é o valor esperado (40 - 4 livros a 10 cada) e o segundo é o valor retornado.
		Assert.assertEquals(new BigDecimal(40).setScale(2), valor);
	}
}
