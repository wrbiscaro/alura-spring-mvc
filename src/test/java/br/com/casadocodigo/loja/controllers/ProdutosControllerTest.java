package br.com.casadocodigo.loja.controllers;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.casadocodigo.loja.conf.AppWebConfiguration;
import br.com.casadocodigo.loja.conf.DataSourceConfigurationTest;
import br.com.casadocodigo.loja.conf.JPAConfiguration;
import br.com.casadocodigo.loja.conf.SecurityConfiguration;

@RunWith(SpringJUnit4ClassRunner.class) //Annotation do JUnit para ele rodar junto com as classes de testes (SpringJUnit4ClassRunner) do Spring
@WebAppConfiguration
@ContextConfiguration(classes={JPAConfiguration.class, AppWebConfiguration.class, DataSourceConfigurationTest.class, SecurityConfiguration.class}) //Informa as classes necessarias para rodar os testes
@ActiveProfiles("test")
public class ProdutosControllerTest {

	@Autowired
	private WebApplicationContext wac; //Contexto da aplicacao
	
	private MockMvc mockMvc; //Mock que finge ser o mvc
	
	@Autowired
	private Filter springSecurityFilterChain; //Filtro do Spring Security para rodar os testes de permissoes
	
	@Before //Anotacao do JUnit que faz um metodo rodar antes dos testes
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).addFilter(springSecurityFilterChain).build();
	}
	
	//Metodo que faz um teste de requisicao para a home, verificando se a jsp foi retornada corretamente junto com os objetos necessarios (produtos)
	@Test
	public void deveRetornarParaHomeComOsLivros() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(MockMvcResultMatchers.model().attributeExists("produtos")).andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/home.jsp")); //O mock simula uma requisicao do tipo get
	}
	
	//Metodo que faz um teste de requisicao para o form, verificando se é o admin que esta tentando acessar
	@Test
	public void somenteAdminDeveAcessarProdutosForm() throws Exception {
		//Tenta acessar com o usuario "user@casadocodigo.com.br" e role "USUARIO", esperando que um erro do tipo 403 (acesso negado) seja retornado
		mockMvc.perform(MockMvcRequestBuilders.get("/produtos/form").with(SecurityMockMvcRequestPostProcessors.user("user@casadocodigo.com.br").password("123456").roles("USUARIO"))).andExpect(MockMvcResultMatchers.status().is(403));
	}
}
