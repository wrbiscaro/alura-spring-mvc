package br.com.casadocodigo.loja.conf;

import java.util.Locale;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

public class JsonViewResolver implements ViewResolver {

	@Override
	public View resolveViewName(String viewName, Locale locale) throws Exception {
		MappingJackson2JsonView jsonView = new MappingJackson2JsonView(); //Classe do Spring que resolve a view, retornando apenas o json (utiliza a biblioteca Jackson)
		jsonView.setPrettyPrint(true); //Deixa o json mais "bonito"
		
		return jsonView;
	}

}
