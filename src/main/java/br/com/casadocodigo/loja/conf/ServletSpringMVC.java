package br.com.casadocodigo.loja.conf;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class ServletSpringMVC extends AbstractAnnotationConfigDispatcherServletInitializer{

	@Override
	protected Class<?>[] getRootConfigClasses() { //Neste metodo as configuracoes sao iniciadas assim que a aplicacao é iniciada
		return new Class[] {SecurityConfiguration.class, AppWebConfiguration.class, JPAConfiguration.class, JPAProductionConfiguration.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() { //Neste metodo as configuracoes sao iniciadas assim que a aplicacao recebe o primeiro acesso
		return new Class[] {};
	}

	@Override
	protected String[] getServletMappings() {
		 return new String[] {"/"};
	}

	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setEncoding("UTF-8");
		
		//CharacterEncodingFilter: filtro de codificacao 
		//OpenEntityManagerInViewFilter: filtro que mantem o entity manager aberto ate a requisicao chegar na view, evitando o LazyInitializationException
		return new Filter[] {encodingFilter, new OpenEntityManagerInViewFilter()};
	}
	
	@Override
	protected void customizeRegistration(Dynamic registration) {
		registration.setMultipartConfig(new MultipartConfigElement(""));
	}
	
	//Metodo com configuracoes de inicializacao. 
	//Metodo comentado que pois faz o perfil ser "dev" ao iniciar. Com o Heroku, o perfil tem que ser "prod" quando for online. 
	//A nova configuracao esta no arquivo "Procfile".
	/*
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
		servletContext.addListener(RequestContextListener.class); //Diz para o Spring ouvir todos os contextos do sistema
		servletContext.setInitParameter("spring.profiles.active", "dev"); //Diz ao Spring que o perfil da aplicacao sera o de dev
	}
	*/
}