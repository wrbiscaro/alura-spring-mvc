package br.com.casadocodigo.loja.conf;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.datetime.DateFormatterRegistrar;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.google.common.cache.CacheBuilder;

import br.com.casadocodigo.loja.controllers.HomeController;
import br.com.casadocodigo.loja.daos.ProdutoDAO;
import br.com.casadocodigo.loja.infra.FileSaver;
import br.com.casadocodigo.loja.models.CarrinhoCompras;

@EnableWebMvc
@ComponentScan(basePackageClasses={HomeController.class, ProdutoDAO.class, FileSaver.class, CarrinhoCompras.class})
@EnableCaching //Habilita o cache para a aplicacao
public class AppWebConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public InternalResourceViewResolver internalResourceViewResolver(){
	    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
	    resolver.setPrefix("/WEB-INF/views/");
	    resolver.setSuffix(".jsp");
	    
	    //Disponibiliza o bean do carrinho de compras nas jsps, podendo assim ser acessado por EL. Obs.: isso é necessario pois o bean do carrinho de compras esta somente no escopo do Spring, nao disponivel assim para as EL.
	    resolver.setExposedContextBeanNames("carrinhoCompras"); //Nome do model
	    
	    return resolver;
	}
	
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/WEB-INF/messages");
		messageSource.setDefaultEncoding("UTF-8");
		messageSource.setCacheSeconds(1);
		
		return messageSource;
	}
	
	@Bean
	public FormattingConversionService mvcConversionService() {
		DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();
		
		DateFormatterRegistrar registrar = new DateFormatterRegistrar();
		registrar.setFormatter(new DateFormatter("dd/MM/yyyy"));
		registrar.registerFormatters(conversionService);
		
		return conversionService;
	}
	
	@Bean
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}
		
	@Bean //Metodo responsavel por criar o RestTemplate para o Spring fazer requisicoes Rest
	public RestTemplate restTemplate() { 
		return new RestTemplate();
	}
	
	@Bean //Metodo responsavel por disponibilizar o gerenciador de cache
	public CacheManager cacheManager() {
		
		//Utilizando a biblioteca Guava (do Google) para gerenciamento do cache, pois o Spring nao possui um gerenciador de cache capaz de receber configuracoes avancadas, como tamanho maximo, timeout, etc
		CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder().maximumSize(100).expireAfterAccess(5, TimeUnit.MINUTES);
		GuavaCacheManager manager = new GuavaCacheManager();
		manager.setCacheBuilder(builder);
		
		return manager;
	}
	
	@Bean //Metodo responsavel por fazer a negociacao e resolver se o metodo retornara uma view ou um json
	public ViewResolver contentNegotiationViewResolver(ContentNegotiationManager manager) {
		
		List<ViewResolver> viewResolvers = new ArrayList<>();
		viewResolvers.add(internalResourceViewResolver()); //Metodo que resolve os retornos de jsp
		viewResolvers.add(new JsonViewResolver()); //Classe responsavel por resolver os retornos em json
		
		ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();		
		resolver.setViewResolvers(viewResolvers);
		resolver.setContentNegotiationManager(manager);
		
		return resolver;
	}
	
	//Habilita a servlet padrao do servidor para receber requisicoes de arquivos (css, js, imagens, etc.), deixando o Spring receber apenas as outras requisicoes
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }	
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		//Adiciona um interceptador de localidade, para receber a opcao de idioma escolhida pelo usuario. O Spring verifica o parametro enviado pelo usuario e procura o arquivo de mensagem correspondente (o nome do parametro tem que ser o mesmo do arquivo de mensagem).
		registry.addInterceptor(new LocaleChangeInterceptor()); 
	}
	
	//Metodo responsavel por carregar a pagina no idioma escolhido e armazenar a opcao em cookie
	@Bean
	public LocaleResolver localeResolver() {
		return new CookieLocaleResolver();
	}
	
	//Configuracao do bean responsavel pelo envio de email
	@Bean
	public MailSender mailSender() {
		//Configuracoes da conta que enviara os emails
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(""); //Host. Ex.: mailSender.setHost("smtp.gmail.com");
		mailSender.setUsername(""); //Usuario
		mailSender.setPassword(""); //Senha
		mailSender.setPort(587); //Porta. Ex.: mailSender.setPort(587) (padrao SMTP)
		
		//Configuracoes de segurança do email (se necessario)
		Properties mailProperties = new Properties();
		mailProperties.put("mail.smtp.auth", true);
		//mailProperties.put("mail.smtp.ssl.trust", "smtp.gmail.com"); //Solucao encontrada para o erro "Could not convert socket to TLS"
		mailProperties.put("mail.smtp.starttls.enable", true);
		mailSender.setJavaMailProperties(mailProperties);
		
		return mailSender;
	}
}