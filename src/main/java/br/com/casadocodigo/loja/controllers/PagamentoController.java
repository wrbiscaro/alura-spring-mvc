package br.com.casadocodigo.loja.controllers;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casadocodigo.loja.models.CarrinhoCompras;
import br.com.casadocodigo.loja.models.DadosPagamento;
import br.com.casadocodigo.loja.models.Usuario;

@Controller
@RequestMapping("/pagamento")
public class PagamentoController {
	
	@Autowired
	private CarrinhoCompras carrinho;
	
	@Autowired
	private RestTemplate restTemplate; //Classe do Spring capaz de fazer requisicoes Rest
	
	@Autowired
	private MailSender sender;
	
	//Callable diz que o metodo sera assincrono, fazendo com que o Tomcat libere outras threads para os demais usuario
	@RequestMapping(value="/finalizar", method=RequestMethod.POST)
	public Callable<ModelAndView> finalizar(@AuthenticationPrincipal Usuario usuario, RedirectAttributes model) {
		
		//O metodo retorna a classe Callable e esta instrucao com return (Lambda do Java 8) é necessaria, para que o bloco de codigos faca parte da classe ao ser retornada e nao seja necessario criar uma classe anonima
		return () -> {		
			String uri = "http://book-payment.herokuapp.com/payment"; //URL para fazer pagamentos ficticios via WebService
			
			try {
				//O Spring cria um JSON automaticamente atraves da biblioteca Jackson e acessa o servico atraves do RestTemplate
				String response = restTemplate.postForObject(uri, new DadosPagamento(carrinho.getTotal()), String.class);
				
				System.out.println(response);
				//enviaEmailCompraProduto(usuario); //Envio de e-mail comentado para nao ficar enviando emails
				model.addFlashAttribute("sucesso", response);
				return new ModelAndView("redirect:/produtos");
			} catch (HttpClientErrorException e) {
				e.printStackTrace();
				model.addFlashAttribute("falha", "Valor maior que o permitido");
				return new ModelAndView("redirect:/produtos");
			}			
			
		};
	}

	private void enviaEmailCompraProduto(Usuario usuario) { //Recebe o usuario que esta logado e foi injetado pelo Spring Security no metodo finalizar(), atraves da annotation @AuthenticationPrincipal
		//Classe de email do Spring
		SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject("Compra finalizada com sucesso");
		email.setTo(usuario.getEmail());
		email.setText("Compra aprovada com sucesso no valor de " + carrinho.getTotal());
		email.setFrom(""); //Conta que sera enviada como destinataria do e-mail
		
		sender.send(email);
	}
}
