package br.com.casadocodigo.loja.controllers;

import javax.persistence.NoResultException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice //Anotação que faz um controller observar todos os outros controllers
public class ExceptionHandlerController {

	//Metodo criado para capturar as exceptions do sistema e enviar para uma pagina de erro
	@ExceptionHandler(Exception.class)
	public ModelAndView trataExceptionGenerica(Exception exception) {
		System.out.println("Erro genérico acontecendo");
		exception.printStackTrace();
		
		ModelAndView modelAndView = new ModelAndView("error");
		modelAndView.addObject("exception", exception);
		
		return modelAndView;
	}
}
