package br.com.casadocodigo.loja.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.casadocodigo.loja.daos.ProdutoDAO;
import br.com.casadocodigo.loja.daos.UsuarioDAO;
import br.com.casadocodigo.loja.models.Produto;
import br.com.casadocodigo.loja.models.Role;
import br.com.casadocodigo.loja.models.Usuario;

@Controller
public class HomeController {

	@Autowired
	private ProdutoDAO produtoDao;
	
	@Autowired
	private UsuarioDAO usuarioDao;
	
    @RequestMapping("/")
    @Cacheable(value="produtosHome") //Torna o metodo cacheavel, ou seja, o Spring faz a consulta ao banco apenas na primeira vez e armazena essas informações no cache
    public ModelAndView index(){
    	List<Produto> produtos = produtoDao.listar();
    	ModelAndView modelAndView = new ModelAndView("home");
    	modelAndView.addObject("produtos", produtos);
    	
    	return modelAndView;
    }
    
    //Metodo que insere o primeiro usuario para inicio da utilizacao do sistema (nao utilizar isso em producao, por questoes de seguranca)
    @Transactional //Torna o metodo transacional, para fazer transacoes com o bd
    @ResponseBody //Retorna a String como String mesmo, e nao uma jsp
    @RequestMapping("/url-magica-maluca-isdhiahfihtiuhuihbnigniusewr")
    public String urlMagicaMaluca() {
    	Usuario usuario = new Usuario();
    	usuario.setNome("Admin");
    	usuario.setEmail("admin@casadocodigo.com.br");
    	usuario.setSenha("$2a$04$qP517gz1KNVEJUTCkUQCY.JzEoXzHFjLAhPQjrg5iP6Z/UmWjvUhq");
    	usuario.setRoles(Arrays.asList(new Role("ROLE_ADMIN")));
    	
    	usuarioDao.gravar(usuario);
    	
    	return "Url Mágica executada";
    }
}