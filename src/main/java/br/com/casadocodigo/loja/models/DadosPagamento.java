package br.com.casadocodigo.loja.models;

import java.math.BigDecimal;

//Classe responsavel por receber um valor e armazenar em um atributo chamado "value", para que o Spring possa pegar o valor atraves do getValue() e enviar via RestTemplate para o WebService de pagamento ficticio.
public class DadosPagamento {

	private BigDecimal value;
	
	public DadosPagamento(BigDecimal value) {
		this.value = value;
	}
	
	//Construtor padrao. É uma boa pratica adicionar um construtor padrao tambem, caso outro modulo da aplicacao necessite utilizar a classe
	public DadosPagamento() {
		
	}
	
	public BigDecimal getValue() {
		return value;
	}
}
