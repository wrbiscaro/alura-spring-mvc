<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %> <!-- Tag de template criada manualmente -->
		
<tags:pageTemplate titulo="Erro genérico acontecendo" >
	<section id="index-section" class="container middle">
	
		<h2>Erro genérico acontecendo!</h2>
		
		<!-- Exibe o erro em um comentario. Caso seja necessario visualizar o que aconteceu, é só inspecionar o elemento e verificar o comentário
		
			Mensagem: ${exception.message} 
			<c:forEach items="${exception.stackTrace}" var="stk">
				${stk}
			</c:forEach>
		-->
		
	</section>
</tags:pageTemplate>
